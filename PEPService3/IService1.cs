﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace PEPService
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IService1" в коде и файле конфигурации.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GeneratePublicKey(string Organization, string TabNum, string FIO, string Karta, string PIN);

        [OperationContract]
        string Authentication(string Karta, string PIN);

        [OperationContract]
        string GenerateTransaction(string DocNum, string DocName, decimal Price, string Place, string Property, string Karta, string PIN, DateTime Date);

        [OperationContract]
        string GenerateTransactionWithHash(string DocNum, string DocName, decimal Price, string Place, string Property, string Hash, DateTime Date);

        [OperationContract]
        string ValidateHash(string DocNum, string DocName, decimal Price, string Place, string Property, string Karta, string PIN, DateTime Date);

        [OperationContract]
        List<Certificates> GetCertificateList();

        [OperationContract]
        List<Transactions> GetTransactionsList(DateTime DateStart, DateTime DateEnd, string Place = "", string CardNum = "", string DocNumber = "");
    }    
}
