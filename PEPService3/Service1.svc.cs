﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PEPService
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде, SVC-файле и файле конфигурации.
    // ПРИМЕЧАНИЕ. Чтобы запустить клиент проверки WCF для тестирования службы, выберите элементы Service1.svc или Service1.svc.cs в обозревателе решений и начните отладку.
    public class Service1 : IService1
    {
        public string GeneratePublicKey(string Organization, string TabNum, string FIO, string Karta, string PIN)
        {
            string Data = Karta + " " + PIN;
            string hashString = GenerateHash(Data);

            try
            {                
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    var Certificat_s = db.Certificates.Where(c => c.PublicKey == hashString && c.IsEnabled).OrderByDescending(c => c.UpdatedAt).ToList();
                    if (Certificat_s != null && Certificat_s.Count > 0)
                    {
                        foreach (var Certificat in Certificat_s)
                        {
                            if (Certificat.CardNum == Karta)
                            {
                                //Проверку на изменение таб. номера сотрудника или организации
                                //if (Certificat.TabNum == TabNum && Certificat.Organization == Organization) //не другой сотрудник или Организация
                                if (Certificat.TabNum == TabNum) //не другой сотрудник
                                {
                                    return hashString;
                                }
                            }
                            else
                            {
                                return "Ошибка: Такой публичный ключ электронной подписи уже существует. Поменяйте ПИН код";
                            }
                        }
                    }

                    foreach (var Certificat in Certificat_s)
                    {
                        Certificat.IsEnabled = false;
                    }

                    db.Certificates.InsertOnSubmit(new Certificates
                    {
                        CardNum = Karta,
                        FullName = FIO,
                        Organization = Organization,
                        PublicKey = hashString,
                        TabNum = TabNum,
                        UpdatedAt = DateTime.Now,
                        IsEnabled = true
                    });
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                return "Ошибка: " + ex.Message + "---" + ex.InnerException;
            }

            return hashString;
        }

        private static string GenerateHash(string Data)
        {
            // вычисляем публичный ключ
            byte[] bytes = Encoding.Unicode.GetBytes(Data);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public string Authentication(string Karta, string PIN)
        {
            string Data = Karta + " " + PIN;
            string hashString = GenerateHash(Data);

            try
            {
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    var Certificat_s = db.Certificates.Where(c => c.PublicKey == hashString && c.IsEnabled).OrderByDescending(c => c.UpdatedAt).ToList();
                    if (Certificat_s != null && Certificat_s.Count > 0)
                    {
                        foreach (var Certificat in Certificat_s)
                        {
                            if (Certificat.CardNum == Karta)
                            {
                                return hashString;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Ошибка: " + ex.Message + "---" + ex.InnerException;
            }
            return "";
        }        

        public string GenerateTransaction(string DocNum, string DocName, decimal Price, string Place, string Property, string Karta, string PIN, DateTime Date)
        {
            string Key = Karta + " " + PIN;
            string PublicKey = GenerateHash(Key);
            if (PublicKey != Authentication(Karta, PIN))
            {
                return "Ошибка: Ошибка авторизации!";
            }
            return TransactionGeneration(DocNum, DocName, Price, Place, Property, Date, Karta, PublicKey);
        }

        private string TransactionGeneration(string DocNum, string DocName, decimal Price, string Place, string Property, DateTime Date, string Karta, string PublicKey)
        {
            string Data = DocName + " " + DocNum + " " + Price.ToString() + " " + Place + " " + Property + " " + PublicKey + " " + Date.ToString();
            string hashString = GenerateHash(Data);

            try
            {
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    db.Transactions.InsertOnSubmit(new Transactions
                    {
                        CardNum = Karta,
                        DocName = DocName,
                        Price = Price,
                        DocNumber = DocNum,
                        Place = Place,
                        Property = Property,
                        CreatedAt = Date,
                        PublicKey = PublicKey,
                        HashCode = hashString
                    });
                    db.SubmitChanges();
                    return hashString;
                }
            }
            catch (Exception ex)
            {
                return "Ошибка: " + ex.Message + "---" + ex.InnerException;
            }
            return hashString;
        }

        public string GenerateTransactionWithHash(string DocNum, string DocName, decimal Price, string Place, string Property, string Hash, DateTime Date)
        {
            try
            {
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    var Certific = db.Certificates.FirstOrDefault(c => c.IsEnabled && c.PublicKey == Hash);
                    if (Certific != null)
                    {
                        return TransactionGeneration(DocNum, DocName, Price, Place, Property, Date, Certific.CardNum, Hash);
                    }
                    else
                    {
                        return "Ошибка: Ошибка авторизации!";
                    }
                }
            }
            catch (Exception ex)
            {
                return "Ошибка: " + ex.Message + "---" + ex.InnerException;
            }
        }

        public string ValidateHash(string DocNum, string DocName, decimal Price, string Place, string Property, string Karta, string PIN, DateTime Date)
        {
            string DataKey = Karta + " " + PIN;
            string PublicKey = GenerateHash(DataKey);

            string Data = DocName + " " + DocNum + " " + Price.ToString() + " " + Place + " " + Property + " " + PublicKey + " " + Date.ToString();
            string HashString = GenerateHash(Data);

            try
            {
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    var Transacs = db.Transactions.Where(c => c.PublicKey == PublicKey && c.HashCode == HashString).OrderByDescending(c => c.CreatedAt).ToList();
                    foreach (var Tran in Transacs)
                    {
                        return Tran.HashCode;
                    }
                }
            }
            catch (Exception ex)
            {
                return "Ошибка: " + ex.Message + "---" + ex.InnerException;
            }
            return "";
        }

        public List<Certificates> GetCertificateList()
        {
            try
            {
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    var Certifics = db.Certificates.Where(c => c.IsEnabled).OrderBy(c => c.FullName).ToList();
                    if (Certifics != null && Certifics.Count > 0)
                        return Certifics;
                }
            }
            catch
            {
                return new List<Certificates>();
            }
            return new List<Certificates>();
        }

        public List<Transactions> GetTransactionsList(DateTime DateStart, DateTime DateEnd, string Place = "", string CardNum = "", string DocNumber = "")
        {
            try
            {
                using (DataClasses1DataContext db = new DataClasses1DataContext())
                {
                    var Transacts = db.Transactions.Where(c => c.CreatedAt >= DateStart && c.CreatedAt <= DateEnd).OrderByDescending(c => c.CreatedAt).ToList();
                    if (Place != "")
                        Transacts.Where(c => c.Place == Place).ToList();
                    if (CardNum != "")
                        Transacts.Where(c => c.CardNum == CardNum).ToList();
                    if (DocNumber != "")
                        Transacts.Where(c => c.DocNumber == DocNumber).ToList();
                    if (Transacts != null && Transacts.Count > 0)
                        return Transacts;
                }
            }
            catch
            {
                return new List<Transactions>();
            }
            return new List<Transactions>();
        }
    }
}
