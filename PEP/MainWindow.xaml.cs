﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Input;

namespace PEP
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string Podrazdelenie = "";
        public MainWindow()
        {
            InitializeComponent();
            AutorizationForm f = new AutorizationForm();
            f.ShowDialog();
            ListCertificates r = new ListCertificates();
            r.Show();
        }


        private void OK_Click(object sender, RoutedEventArgs e)
        {
            if (Number.Text == String.Empty) { MessageBox.Show("Введите данные УЭКР!"); return; }
            try { Convert.ToInt32(Number.Text); } catch { MessageBox.Show("Серия+Номер УЭКР должны содержать только цифры!"); return; }
            if (Number.Text.Length != 8) { MessageBox.Show("Серия+Номер УЭКР должны состоять из 8 цифр!"); return; }
            if (Organization.Text == String.Empty) { MessageBox.Show("Укажите место работы работника!"); return; };
            if (FIO.Text == String.Empty) { MessageBox.Show("Введите ФИО работника!"); return; }
            if (TabNomer.Text == String.Empty) { MessageBox.Show("Введите табельный номер работника!"); return; }
            try { Convert.ToInt32(TabNomer.Text); } catch { MessageBox.Show("Табельный номер должен содержать только цифры!"); return; }
            if (PIN.Password == String.Empty) { MessageBox.Show("Работник не ввел ПИН код!"); return; }
            try { Convert.ToInt32(PIN.Password); } catch { MessageBox.Show("ПИН код должен содержать только цифры!"); return; }
            if (PIN.Password.Length < 4) { MessageBox.Show("ПИН код должен состоять из не менее 4-х цифр!"); return; }
            if (PINRepeat.Password != PIN.Password)
            {
                PIN.Password = String.Empty;
                PINRepeat.Password = String.Empty;
                PIN.Focus();
                MessageBox.Show("ПИН коды должны совпадать!"); return;
            }

            try
            {
                var client = new ServiceReference.Service1Client();
                string PublicKey = client.GeneratePublicKey(Organization.Text, TabNomer.Text, FIO.Text, Number.Text, PIN.Password);
                string Hash = client.Authentication(Number.Text, PIN.Password);
                if (PublicKey == Hash)
                {
                    MessageBox.Show("Пин код добавлен/обновлен!");
                    Number.Text = String.Empty;
                    Organization.Text = String.Empty;
                    FIO.Text = String.Empty;
                    TabNomer.Text = String.Empty;
                    PIN.Password = String.Empty;
                    PINRepeat.Password = String.Empty;
                    Number.Focus();
                }                    
                else
                    MessageBox.Show("Ошибка при добавление ПИН кода!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            AddNewOrganization();
        }

        private void AddNewOrganization()
        {
            using (dbDataContext db = new dbDataContext())
            {
                var Orgs = db.Organizations.FirstOrDefault(c => c.Name == Organization.Text);
                if (Orgs == null)
                {
                    db.Organizations.InsertOnSubmit(new Organizations
                    {
                        Name = Organization.Text
                    });
                    db.SubmitChanges();
                    var Organiz = db.Organizations.Select(c => c.Name).ToList();
                    Organization.ItemsSource = Organiz;
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            using (dbDataContext db = new dbDataContext())
            {
                var Orgs = db.Organizations.Select(c => c.Name).ToList();
                Organization.ItemsSource = Orgs;
            }
            Number.Focus();
        }

        private void Number_KeyUp(object sender, KeyEventArgs e)
        {
            Number.Text = Number.Text.Replace(" ", "").Replace("\n", "").Replace("\r", "");
            if (Number.Text.Length == 8)
            {
                Podrazdelenie = "";
                PIN.Password = String.Empty;
                PINRepeat.Password = String.Empty;
                TabNomer.Text = String.Empty;
                FIO.Text = String.Empty;
                Organization.Text = String.Empty;
                //SMPNEFTEGAZ
                try
                {
                    var SMP = new SMPNEFTEGAZ.el_karta();
                    var SMPData = SMP.poiskFood(Number.Text, true);
                    if (SMPData != null && SMPData.Length > 0)
                    {
                        TabNomer.Text = SMPData[0].tab;
                        FIO.Text = SMPData[0].fio;
                        Podrazdelenie = SMPData[0].podrazd;
                        Organization.Text = "АО \"СМП-Нефтегаз\"";
                        SetOrganization("Нефтегаз");
                        PIN.Focus();
                        return;
                    }
                }
                catch (Exception ex) { }
                //ALSU
                try
                {
                    var ALSUR = new ALSU.el_shop();
                    var ALSUData = ALSUR.poiskFood(Number.Text);
                    if (ALSUData != null && ALSUData.Length > 0)
                    {
                        TabNomer.Text = ALSUData[0].tab;
                        FIO.Text = ALSUData[0].fio;
                        Podrazdelenie = ALSUData[0].podrazd;
                        Organization.Text = "АО \"АЧНФ \"Алсу\"";
                        SetOrganization("АЛСУ");
                        PIN.Focus();
                        return;
                    }
                }
                catch (Exception ex) { }
                //TRANSOIL
                try
                {
                    var TRANSOILR = new TRANSOIL.el_karta();//el_kartaPortTypeClient("el_kartaSoap");
                    var TRANSOILData = TRANSOILR.poiskFood(Number.Text);
                    if (TRANSOILData != null && TRANSOILData.Length > 0)
                    {
                        TabNomer.Text = TRANSOILData[0].tab;
                        FIO.Text = TRANSOILData[0].fio;
                        Podrazdelenie = TRANSOILData[0].podrazd;
                        Organization.Text = "ООО \"Трансойл\"";
                        SetOrganization("Трансо");
                        PIN.Focus();
                        return;
                    }
                }
                catch (Exception ex) { }
                //AUTOSTRADA
                try
                {
                    var AUTOSTRADAR = new AVTOSTRADA.el_karta();
                    AUTOSTRADAR.Credentials = new NetworkCredential("web", "321321321");
                    var AUTOSTRADAData = AUTOSTRADAR.poiskFood(Number.Text);
                    if (AUTOSTRADAData != null && AUTOSTRADAData.Length > 0)
                    {
                        TabNomer.Text = AUTOSTRADAData[0].tab;
                        FIO.Text = AUTOSTRADAData[0].fio;
                        Podrazdelenie = AUTOSTRADAData[0].podrazd;
                        Organization.Text = "АО \"Автострада\"";
                        SetOrganization("Автострада");
                        PIN.Focus();
                        return;
                    }
                }
                catch (Exception ex) { }
                //TOKARLIKOVA
                try
                {
                    var TOKARLIKOVAR = new TOKARLIKOVA.el_karta();
                    TOKARLIKOVAR.Credentials = new NetworkCredential("web", "321321321");
                    var TOKARLIKOVAData = TOKARLIKOVAR.poiskFood(Number.Text);
                    if (TOKARLIKOVAData != null && TOKARLIKOVAData.Length > 0)
                    {
                        TabNomer.Text = TOKARLIKOVAData[0].tab;
                        FIO.Text = TOKARLIKOVAData[0].fio;
                        Podrazdelenie = TOKARLIKOVAData[0].podrazd;
                        Organization.Text = "АО \"им. Н.Е. Токарликова\"";
                        SetOrganization("Токарликов");
                        PIN.Focus();
                        return;
                    }
                    if (e.Key == Key.Enter || e.Key == Key.Tab)
                    {
                        Organization.Focus();
                    }
                }
                catch (Exception ex) { }
            }
        }

        private void SetOrganization(string Primer)
        {
            Primer = Primer.ToUpper();
            using (dbDataContext db = new dbDataContext())
            {
                var Organiz = db.Organizations.Select(c => c.Name).ToList();
                foreach (var Org in Organiz)
                {
                    if (Org.ToUpper().Contains(Primer))
                    {
                        Organization.Text = Org;
                        break;
                    }
                }
            }
        }

        private void PIN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter || e.Key == Key.Tab)
                {
                    if(PIN.Password.Length >= 4)
                        PINRepeat.Focus();
                }
            }
            catch { }
        }

        private void PINRepeat_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter || e.Key == Key.Tab)
                {
                    if(PIN.Password != PINRepeat.Password)
                    {
                        PIN.Password = String.Empty;
                        PINRepeat.Password = String.Empty;
                        PIN.Focus();
                        MessageBox.Show("ПИН коды должны совпадать!");
                    }
                    else                    
                        OK.Focus();
                }
            }
            catch { }
        }

        private void Organization_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                FIO.Focus();
            }
        }

        private void FIO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TabNomer.Focus();
            }
        }

        private void TabNomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                PIN.Focus();
            }
        }
    }
}
