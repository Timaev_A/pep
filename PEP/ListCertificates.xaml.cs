﻿using System.Linq;
using System.Windows;

namespace PEP
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class ListCertificates : Window
    {        
        public ListCertificates()
        {
            InitializeComponent();                        
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            using (dbDataContext db = new dbDataContext())
            {
                var Cert = db.Certificates.Where(c => c.IsEnabled == false).ToList();
                Tabl. = Cert;
            }
            Tabl.Focus();
        }       

    }
}
