﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// Этот исходный текст был создан автоматически: Microsoft.VSDesigner, версия: 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace PEP.AVTOSTRADA {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="el_kartaSoapBinding", Namespace="http://db-5/web_service/ws")]
    public partial class el_karta : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback poiskFoodOperationCompleted;
        
        private System.Threading.SendOrPostCallback poiskTISOperationCompleted;
        
        private System.Threading.SendOrPostCallback Raschet_listOperationCompleted;
        
        private System.Threading.SendOrPostCallback PoluchitDannieSotrOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public el_karta() {
            this.Url = global::PEP.Properties.Settings.Default.PEP_AVTOSTRADA_el_karta;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event poiskFoodCompletedEventHandler poiskFoodCompleted;
        
        /// <remarks/>
        public event poiskTISCompletedEventHandler poiskTISCompleted;
        
        /// <remarks/>
        public event Raschet_listCompletedEventHandler Raschet_listCompleted;
        
        /// <remarks/>
        public event PoluchitDannieSotrCompletedEventHandler PoluchitDannieSotrCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://db-5/web_service/ws#el_karta:poiskFood", RequestNamespace="http://db-5/web_service/ws", ResponseNamespace="http://db-5/web_service/ws", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlArrayAttribute("return", IsNullable=true)]
        [return: System.Xml.Serialization.XmlArrayItemAttribute("elements", Namespace="http://www.sample-package5.org", IsNullable=false)]
        public Getpeople[] poiskFood(string shtrihkod) {
            object[] results = this.Invoke("poiskFood", new object[] {
                        shtrihkod});
            return ((Getpeople[])(results[0]));
        }
        
        /// <remarks/>
        public void poiskFoodAsync(string shtrihkod) {
            this.poiskFoodAsync(shtrihkod, null);
        }
        
        /// <remarks/>
        public void poiskFoodAsync(string shtrihkod, object userState) {
            if ((this.poiskFoodOperationCompleted == null)) {
                this.poiskFoodOperationCompleted = new System.Threading.SendOrPostCallback(this.OnpoiskFoodOperationCompleted);
            }
            this.InvokeAsync("poiskFood", new object[] {
                        shtrihkod}, this.poiskFoodOperationCompleted, userState);
        }
        
        private void OnpoiskFoodOperationCompleted(object arg) {
            if ((this.poiskFoodCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.poiskFoodCompleted(this, new poiskFoodCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://db-5/web_service/ws#el_karta:poiskTIS", RequestNamespace="http://db-5/web_service/ws", ResponseNamespace="http://db-5/web_service/ws", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public string poiskTIS(string shtrihkod) {
            object[] results = this.Invoke("poiskTIS", new object[] {
                        shtrihkod});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void poiskTISAsync(string shtrihkod) {
            this.poiskTISAsync(shtrihkod, null);
        }
        
        /// <remarks/>
        public void poiskTISAsync(string shtrihkod, object userState) {
            if ((this.poiskTISOperationCompleted == null)) {
                this.poiskTISOperationCompleted = new System.Threading.SendOrPostCallback(this.OnpoiskTISOperationCompleted);
            }
            this.InvokeAsync("poiskTIS", new object[] {
                        shtrihkod}, this.poiskTISOperationCompleted, userState);
        }
        
        private void OnpoiskTISOperationCompleted(object arg) {
            if ((this.poiskTISCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.poiskTISCompleted(this, new poiskTISCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://db-5/web_service/ws#el_karta:Raschet_list", RequestNamespace="http://db-5/web_service/ws", ResponseNamespace="http://db-5/web_service/ws", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public File Raschet_list(string tab, string Period) {
            object[] results = this.Invoke("Raschet_list", new object[] {
                        tab,
                        Period});
            return ((File)(results[0]));
        }
        
        /// <remarks/>
        public void Raschet_listAsync(string tab, string Period) {
            this.Raschet_listAsync(tab, Period, null);
        }
        
        /// <remarks/>
        public void Raschet_listAsync(string tab, string Period, object userState) {
            if ((this.Raschet_listOperationCompleted == null)) {
                this.Raschet_listOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRaschet_listOperationCompleted);
            }
            this.InvokeAsync("Raschet_list", new object[] {
                        tab,
                        Period}, this.Raschet_listOperationCompleted, userState);
        }
        
        private void OnRaschet_listOperationCompleted(object arg) {
            if ((this.Raschet_listCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.Raschet_listCompleted(this, new Raschet_listCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://db-5/web_service/ws#el_karta:PoluchitDannieSotr", RequestNamespace="http://db-5/web_service/ws", ResponseNamespace="http://db-5/web_service/ws", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public string PoluchitDannieSotr(string ФамилияИмя) {
            object[] results = this.Invoke("PoluchitDannieSotr", new object[] {
                        ФамилияИмя});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void PoluchitDannieSotrAsync(string ФамилияИмя) {
            this.PoluchitDannieSotrAsync(ФамилияИмя, null);
        }
        
        /// <remarks/>
        public void PoluchitDannieSotrAsync(string ФамилияИмя, object userState) {
            if ((this.PoluchitDannieSotrOperationCompleted == null)) {
                this.PoluchitDannieSotrOperationCompleted = new System.Threading.SendOrPostCallback(this.OnPoluchitDannieSotrOperationCompleted);
            }
            this.InvokeAsync("PoluchitDannieSotr", new object[] {
                        ФамилияИмя}, this.PoluchitDannieSotrOperationCompleted, userState);
        }
        
        private void OnPoluchitDannieSotrOperationCompleted(object arg) {
            if ((this.PoluchitDannieSotrCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.PoluchitDannieSotrCompleted(this, new PoluchitDannieSotrCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.sample-package5.org")]
    public partial class Getpeople {
        
        private string orgField;
        
        private string tabField;
        
        private string fioField;
        
        private string podrazdField;
        
        private string dolgnostField;
        
        private string dataPriemaField;
        
        /// <remarks/>
        public string org {
            get {
                return this.orgField;
            }
            set {
                this.orgField = value;
            }
        }
        
        /// <remarks/>
        public string tab {
            get {
                return this.tabField;
            }
            set {
                this.tabField = value;
            }
        }
        
        /// <remarks/>
        public string fio {
            get {
                return this.fioField;
            }
            set {
                this.fioField = value;
            }
        }
        
        /// <remarks/>
        public string podrazd {
            get {
                return this.podrazdField;
            }
            set {
                this.podrazdField = value;
            }
        }
        
        /// <remarks/>
        public string dolgnost {
            get {
                return this.dolgnostField;
            }
            set {
                this.dolgnostField = value;
            }
        }
        
        /// <remarks/>
        public string dataPriema {
            get {
                return this.dataPriemaField;
            }
            set {
                this.dataPriemaField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.test-ws.org")]
    public partial class File {
        
        private byte[] binaryDataField;
        
        private string extField;
        
        private string messageField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] BinaryData {
            get {
                return this.binaryDataField;
            }
            set {
                this.binaryDataField = value;
            }
        }
        
        /// <remarks/>
        public string ext {
            get {
                return this.extField;
            }
            set {
                this.extField = value;
            }
        }
        
        /// <remarks/>
        public string message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void poiskFoodCompletedEventHandler(object sender, poiskFoodCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class poiskFoodCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal poiskFoodCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Getpeople[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Getpeople[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void poiskTISCompletedEventHandler(object sender, poiskTISCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class poiskTISCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal poiskTISCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void Raschet_listCompletedEventHandler(object sender, Raschet_listCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class Raschet_listCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal Raschet_listCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public File Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((File)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void PoluchitDannieSotrCompletedEventHandler(object sender, PoluchitDannieSotrCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class PoluchitDannieSotrCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal PoluchitDannieSotrCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591