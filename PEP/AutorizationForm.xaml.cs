﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace PEP
{
    /// <summary>
    /// Логика взаимодействия для AutorizationForm.xaml
    /// </summary>
    public partial class AutorizationForm : Window
    {
        string hash = "";
        public AutorizationForm()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var db = new dbDataContext())
                {
                    var UserData = db.Users.Where(c => c.UserName == Login.Text).FirstOrDefault();
                    if (UserData == null)
                    {
                        MessageBox.Show("Неверное имя пользователя и (или) пароль!", "Ошибка авторизации!");
                        return;
                    }
                    if (UserData.NewPassword != null && UserData.NewPassword != string.Empty)
                    {
                        UserData.Password = GenerateHash(UserData.NewPassword);
                        hash = UserData.Password;
                        UserData.NewPassword = null;
                        db.SubmitChanges();
                        Close();
                    }
                    else
                    {
                        if (GenerateHash(Password.Password) == UserData.Password)
                        {
                            hash = UserData.Password;
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Неверное имя пользователя и (или) пароль!", "Ошибка авторизации!");
                            return;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private static string GenerateHash(string Data)
        {
            // вычисляем публичный ключ
            byte[] bytes = Encoding.Unicode.GetBytes(Data);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (hash == "")
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Login.Focus();
        }

        private void Login_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter || e.Key == Key.Tab)
                {
                    Password.Focus();
                }
            }
            catch { }
        }

        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter || e.Key == Key.Tab)
                {
                    OK.Focus();
                }
            }
            catch { }
        }
    }
}
